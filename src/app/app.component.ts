import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(public translateService: TranslateService, private titleService: Title) {
    translateService.setDefaultLang('cs');
    translateService.use('cs');

    translateService.get('PAGE.TITLE').toPromise().then(title => titleService.setTitle(title));
  }
}
