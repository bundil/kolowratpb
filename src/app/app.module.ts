import {NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {VotingListComponent} from './components/voting-list/voting-list';
import {VotingComponent} from './components/voting/voting';
import {VotingResultComponent} from './components/voting-result/voting-result';

import {VotingService} from './components/voting.service';

import {BackButtonComponent} from './components/utils/back-button';
import {ModalVotingOptionDescriptionComponent} from './components/utils/modal-voting-option-description';

import {NumberToTextPipe} from './components/utils/numberToText.pipe';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppComponent,
    VotingListComponent,
    VotingComponent,
    VotingResultComponent,
    BackButtonComponent,
    ModalVotingOptionDescriptionComponent,
    NumberToTextPipe
  ],
  entryComponents: [
    ModalVotingOptionDescriptionComponent
  ],
  providers: [
    VotingService,
    NumberToTextPipe,
    Title
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
