import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {VotingListComponent} from './components/voting-list/voting-list';
import {VotingComponent} from './components/voting/voting';
import {VotingResultComponent} from './components/voting-result/voting-result';

const routes: Routes = [
  {path: '', redirectTo: '/votingList', pathMatch: 'full'},
  {path: 'votingList', component: VotingListComponent},
  {path: 'voting/:votingCode', component: VotingComponent},
  {path: 'votingResult/:votingCode', component: VotingResultComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
