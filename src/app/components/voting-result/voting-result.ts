import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Voting} from '../types/voting';
import {VotingOption} from '../types/votingOption';
import {VotingService} from '../voting.service';

import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ModalVotingOptionDescriptionComponent} from '../utils/modal-voting-option-description';

@Component({
  selector: 'voting-result',
  templateUrl: './voting-result.html'
})

export class VotingResultComponent implements OnInit {
  viewState: string;
  errorText: string;

  votingCode: string;
  voting: Voting;
  votingOptionList: VotingOption[];
  votingUserCount: number;

  votingDetailLoaded: boolean;
  votingOptionListLoaded: boolean;

  votePlusMinusCountMax: number;
  voteSumCountMax: number;
  showVoteCount: string;

  constructor(private votingService: VotingService, private route: ActivatedRoute, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.viewState = 'loading';
    this.votingDetailLoaded = false;
    this.votingOptionListLoaded = false;
    this.votePlusMinusCountMax = -1;
    this.voteSumCountMax = -1;
    this.showVoteCount = 'sum';

    this.route.params.subscribe(params => {
      this.votingCode = params['votingCode'];

      this.getVotingDetail(this.votingCode);
      this.getVotingOptionListResult(this.votingCode);
    });
  }

  getVotingOptionListResult(votingCode: string): void {
    this.votingService.getVotingOptionListResult(votingCode)
      .then(votingResult => {
        this.votingOptionList = votingResult.votingOptionList;
        this.votingUserCount = votingResult.votingUserCount;

        for (const votingOption of this.votingOptionList) {
          votingOption.votePlusCount = parseInt('' + votingOption.votePlusCount, 10);
          votingOption.voteMinusCount = parseInt('' + votingOption.voteMinusCount, 10);

          if (votingOption.votePlusCount > this.votePlusMinusCountMax) {
            this.votePlusMinusCountMax = votingOption.votePlusCount;
          }
          if (votingOption.voteMinusCount > this.votePlusMinusCountMax) {
            this.votePlusMinusCountMax = votingOption.voteMinusCount;
          }
          if (Math.abs(votingOption.votePlusCount - votingOption.voteMinusCount) > this.voteSumCountMax) {
            this.voteSumCountMax = Math.abs(votingOption.votePlusCount - votingOption.voteMinusCount);
          }

          votingOption.voteCount = votingOption.votePlusCount - votingOption.voteMinusCount;
          votingOption.voteCountAbs = Math.abs(votingOption.voteCount);
          votingOption.isPlus = (votingOption.votePlusCount - votingOption.voteMinusCount) >= 0;
          votingOption.isMinus = (votingOption.votePlusCount - votingOption.voteMinusCount) < 0;
          votingOption.isZero = (votingOption.votePlusCount - votingOption.voteMinusCount) === 0;
        }

        this.votingOptionListLoaded = true;
        if (this.votingDetailLoaded && this.votingOptionListLoaded) {
          this.viewState = 'view';
        }
      })
      .catch(err => {
        this.errorText = err.error;
        this.viewState = 'error';
      });
  }

  getVotingDetail(votingCode: string): void {
    this.votingService.getVotingDetail(votingCode)
      .then(voting => {
        this.voting = voting;
        this.votingDetailLoaded = true;
        if (this.votingDetailLoaded && this.votingOptionListLoaded) {
          this.viewState = 'view';
        }
      })
      .catch(err => {
        this.errorText = err.error;
        this.viewState = 'error';
      });
  }

  infoVotingOptionDescription(i) {
    const options: NgbModalOptions = {
      size: 'lg'
    };
    const modalRef = this.modalService.open(ModalVotingOptionDescriptionComponent, options);
    modalRef.componentInstance.infoVotingOptionDescriptionTitle = this.votingOptionList[i].option_text;
    modalRef.componentInstance.infoVotingOptionDescriptionBody = this.votingOptionList[i].option_description;
  }
}
