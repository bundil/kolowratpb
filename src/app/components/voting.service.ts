import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import 'rxjs/add/operator/toPromise';

import {Voting} from './types/voting';
import {VotingOption} from './types/votingOption';
import {VoteNew} from './types/voteNew';
import {VotingResult} from './types/votingResult';
import {VotingUserNew} from './types/votingUserNew';
import {VotingList} from './types/votingList';
import {environment} from '../../environments/environment';

@Injectable()
export class VotingService {

  private restHost = environment.restHost;

  private votingListUrl = this.restHost + '/php/votingList.php';
  private votingOptionListUrl = this.restHost + '/php/votingOptionList.php';
  private votingOptionListResultUrl = this.restHost + '/php/votingOptionListResult.php';
  private votingDetailUrl = this.restHost + '/php/votingDetail.php';
  private votingUserNewUrl = this.restHost + '/php/votingUserNew.php';
  private voteNewUrl = this.restHost + '/php/voteNew.php';

  constructor(private http: HttpClient) {
  }

  getVotingList(): Promise<VotingList> {
    return this.http.get<VotingList>(this.votingListUrl)
      .toPromise();
  }

  getVotingOptionList(votingCode: string): Promise<VotingOption[]> {
    return this.http.post<VotingOption[]>(this.votingOptionListUrl, JSON.stringify({votingCode: votingCode}))
      .toPromise();
  }

  getVotingOptionListResult(votingCode: string): Promise<VotingResult> {
    return this.http.post<VotingResult>(this.votingOptionListResultUrl, JSON.stringify({votingCode: votingCode}))
      .toPromise();
  }

  getVotingDetail(votingCode: string): Promise<Voting> {
    return this.http.post<Voting>(this.votingDetailUrl, JSON.stringify({votingCode: votingCode}))
      .toPromise();
  }

  votingUserNew(votingUserNew: VotingUserNew): Promise<boolean> {
    return this.http.post<boolean>(this.votingUserNewUrl, JSON.stringify(votingUserNew))
      .toPromise();
  }

  voteNew(voteNew: VoteNew): Promise<boolean> {
    return this.http.post<boolean>(this.voteNewUrl, JSON.stringify(voteNew))
      .toPromise();
  }
}

