import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({name: 'numberToText'})
export class NumberToTextPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {

  }

  transform(value: number, inflect: number): string {
    switch (inflect) {
      case 1:
        switch (value) {
          case 1:
            return this.translateService.instant('ONE');
          case 2:
            return this.translateService.instant('TWO');
          case 3:
            return this.translateService.instant('THREE');
          case 4:
            return this.translateService.instant('FOUR');
          case 5:
            return this.translateService.instant('FIVE');
          case 6:
            return this.translateService.instant('SIX');
          case 7:
            return this.translateService.instant('SEVEN');
          case 8:
            return this.translateService.instant('EIGHT');
          case 9:
            return this.translateService.instant('NINE');
          case 10:
            return this.translateService.instant('TEN');
        }
        break;

      case 2:
        switch (value) {
          case 1:
            return this.translateService.instant('ONE_INFLECT_2');
          case 2:
            return this.translateService.instant('TWO_INFLECT_2');
          case 3:
            return this.translateService.instant('THREE_INFLECT_2');
          case 4:
            return this.translateService.instant('FOUR_INFLECT_2');
          case 5:
            return this.translateService.instant('FIVE_INFLECT_2');
          case 6:
            return this.translateService.instant('SIX_INFLECT_2');
          case 7:
            return this.translateService.instant('SEVEN_INFLECT_2');
          case 8:
            return this.translateService.instant('EIGHT_INFLECT_2');
          case 9:
            return this.translateService.instant('NINE_INFLECT_2');
          case 10:
            return this.translateService.instant('TEN_INFLECT_2');
        }
        break;
    }
  }
}
