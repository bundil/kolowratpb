import {Component, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'modal-voting-option-description',
  templateUrl: './modal-voting-option-description.html'
})

export class ModalVotingOptionDescriptionComponent {
  @Input() infoVotingOptionDescriptionTitle;
  @Input() infoVotingOptionDescriptionBody;

  constructor(public activeModal: NgbActiveModal) {
  }
}
