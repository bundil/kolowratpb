import {VotingOption} from './votingOption';

export class VotingResult {
  votingOptionList: VotingOption[];
  votingUserCount: number;
}
