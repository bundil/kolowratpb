export class Voting {
  code: string;
  question: string;
  text: string;
  start: string;
  end: string;
  created: string;
}
