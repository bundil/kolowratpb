export class VoteNew {
  votePlusArr: number[];
  voteMinusArr: number[];
  votingCode: string;
  email: string;
  pin: string;
}
