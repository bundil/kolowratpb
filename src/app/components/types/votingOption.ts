export class VotingOption {
  id: number;
  option_text: string;
  option_description: string;

  // for voting view
  votePlusSelected: boolean;
  voteMinusSelected: boolean;

  votePlusError: boolean;
  voteMinusError: boolean;

  votePlusErrorLong: boolean;
  voteMinusErrorLong: boolean;

  // for result view
  votePlusCount: number;
  voteMinusCount: number;

  voteCount: number;
  voteCountAbs: number;
  isPlus: boolean;
  isMinus: boolean;
  isZero: boolean;
}
