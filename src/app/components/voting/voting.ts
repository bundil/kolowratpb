import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Voting} from '../types/voting';
import {VotingOption} from '../types/votingOption';
import {VoteNew} from '../types/voteNew';
import {VotingService} from '../voting.service';

import {NumberToTextPipe} from '../utils/numberToText.pipe';
import {VotingUserNew} from '../types/votingUserNew';

import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ModalVotingOptionDescriptionComponent} from '../utils/modal-voting-option-description';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'voting',
  templateUrl: './voting.html'
})

export class VotingComponent implements OnInit {
  viewState: string;
  pinState: string;
  errorText: string;
  errorTextInErrorView: string;

  email: string;
  pin: string;

  votingCode: string;
  voting: Voting;
  votingOptionList: VotingOption[];
  votingUserNew: VotingUserNew;
  voteNew: VoteNew;

  VOTE_PLUS_MAX = 2;
  VOTE_MINUS_MAX = 1;

  votingDetailLoaded: boolean;
  votingOptionListLoaded: boolean;

  constructor(private votingService: VotingService, private route: ActivatedRoute, private numberToText: NumberToTextPipe,
              private modalService: NgbModal, private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.viewState = 'loading';
    this.pinState = 'toSend';
    this.votingDetailLoaded = false;
    this.votingOptionListLoaded = false;

    this.email = '';
    this.pin = '';

    this.votingUserNew = new VotingUserNew();

    this.voteNew = new VoteNew();
    this.voteNew.votePlusArr = [];
    this.voteNew.voteMinusArr = [];

    this.route.params.subscribe(params => {
      this.votingCode = params['votingCode'];

      this.getVotingDetail(this.votingCode);
      this.getVotingOptionList(this.votingCode);
    });
  }

  getVotingOptionList(votingCode: string): void {
    this.votingService.getVotingOptionList(votingCode)
      .then(votingOptionList => {
        this.votingOptionList = votingOptionList;
        this.votingOptionListLoaded = true;
        if (this.votingDetailLoaded && this.votingOptionListLoaded) {
          this.viewState = 'view';
        }
      })
      .catch(err => {
        this.errorTextInErrorView = err.error;
        this.viewState = 'error';
      });
  }

  getVotingDetail(votingCode: string): void {
    this.votingService.getVotingDetail(votingCode)
      .then(voting => {
        this.voting = voting;
        this.votingDetailLoaded = true;
        if (this.votingDetailLoaded && this.votingOptionListLoaded) {
          this.viewState = 'view';
        }
      })
      .catch(err => {
        this.errorTextInErrorView = err.error;
        this.viewState = 'error';
      });
  }

  votePlusMinus(votingOption: VotingOption, votePrimaryArr: number[], voteSecondaryArr: number[], isPrimaryPlus: boolean) {
    const VOTE_MAX: number = isPrimaryPlus ? this.VOTE_PLUS_MAX : this.VOTE_MINUS_MAX;

    if (votePrimaryArr.indexOf(votingOption.id) >= 0) {
      votePrimaryArr.splice(votePrimaryArr.indexOf(votingOption.id), 1);
      isPrimaryPlus ? votingOption.votePlusSelected = false : votingOption.voteMinusSelected = false;
    } else {
      if (votePrimaryArr.length < VOTE_MAX) {
        votePrimaryArr.push(votingOption.id);
        isPrimaryPlus ? votingOption.votePlusSelected = true : votingOption.voteMinusSelected = true;

        if (voteSecondaryArr.indexOf(votingOption.id) >= 0) {
          voteSecondaryArr.splice(voteSecondaryArr.indexOf(votingOption.id), 1);
          isPrimaryPlus ? votingOption.voteMinusSelected = false : votingOption.votePlusSelected = false;
        }
      } else {
        isPrimaryPlus ? votingOption.votePlusError = true : votingOption.voteMinusError = true;

        setTimeout(() => {
          if (isPrimaryPlus) {
            votingOption.votePlusError = false;
            votingOption.votePlusErrorLong = true;
          } else {
            votingOption.voteMinusError = false;
            votingOption.voteMinusErrorLong = true;
          }
        }, 500);
        setTimeout(() => {
          isPrimaryPlus ? votingOption.votePlusErrorLong = false : votingOption.voteMinusErrorLong = false;
        }, 2500);
      }
    }
  }

  votePlus(votingOption: VotingOption) {
    this.votePlusMinus(votingOption, this.voteNew.votePlusArr, this.voteNew.voteMinusArr, true);
  }

  voteMinus(votingOption: VotingOption) {
    this.votePlusMinus(votingOption, this.voteNew.voteMinusArr, this.voteNew.votePlusArr, false);
  }

  sendPin() {
    this.closeError();

    this.votingUserNew.votingCode = this.votingCode;
    this.votingUserNew.email = this.email;

    this.pinState = 'sending';

    this.votingService.votingUserNew(this.votingUserNew)
      .then(() => {
        this.pinState = 'sent';
      })
      .catch(err => {
        this.errorText = err.error;
        this.pinState = 'toSend';
      });
  }

  voteNewSend() {
    this.closeError();

    if (this.voteNew.votePlusArr.length + this.voteNew.voteMinusArr.length === 0) {
      this.translateService.get('VOTING.MUST-HAVE-VOTE')
        .toPromise()
        .then(text => this.errorText = text);
      return;
    }
    if ((this.voteNew.voteMinusArr.length * 2) > this.voteNew.votePlusArr.length) {
      this.translateService.get('VOTING.MINUS-SHOULD-HAVE-PLUS',
        {value: this.numberToText.transform(this.VOTE_PLUS_MAX, 2)})
        .toPromise()
        .then(text => this.errorText = text);
      return;
    }
    this.viewState = 'loading';

    this.voteNew.votingCode = this.votingCode;
    this.voteNew.email = this.email;
    this.voteNew.pin = this.pin;
    this.votingService.voteNew(this.voteNew)
      .then(() => {
        this.viewState = 'completed';
      })
      .catch(err => {
        this.errorText = err.error;
        this.viewState = 'view';
      });
  }

  info(content) {
    this.modalService.open(content);
  }

  infoVotingOptionDescription(i) {
    const options: NgbModalOptions = {
      size: 'lg'
    };
    const modalRef = this.modalService.open(ModalVotingOptionDescriptionComponent, options);
    modalRef.componentInstance.infoVotingOptionDescriptionTitle = this.votingOptionList[i].option_text;
    modalRef.componentInstance.infoVotingOptionDescriptionBody = this.votingOptionList[i].option_description;
  }

  closeError() {
    this.errorText = '';
  }
}
