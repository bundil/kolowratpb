import {Component, OnInit} from '@angular/core';

import {Voting} from '../types/voting';
import {VotingService} from '../voting.service';

@Component({
  selector: 'voting-list',
  templateUrl: './voting-list.html'
})

export class VotingListComponent implements OnInit {
  votingListActual: Voting[];
  votingListEnded: Voting[];
  viewState: string;
  errorText: string;

  constructor(private votingService: VotingService) {
  }

  getVotingList(): void {
    this.votingService.getVotingList()
      .then(votingList => {
        this.votingListActual = votingList.votingListActual;
        this.votingListEnded = votingList.votingListEnded;
        this.viewState = 'view';
      })
      .catch(err => {
        this.errorText = err.error;
        this.viewState = 'error';
      });
  }

  ngOnInit(): void {
    this.viewState = 'loading';
    this.getVotingList();
  }
}
