<?php

class Constant
{
  const VOTE_PLUS_MAX = 2;
  const VOTE_MINUS_MAX = 1;
  const PIN_ATTEMPT_MAX = 20;
}
