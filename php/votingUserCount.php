<?php
include('VotingDao.class.php');

try {
  $code = $_GET['votingCode'];

  $votingDao = new VotingDao();

  //validate - voting with code exists
  $votingDao->getVotingId($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  //counts voting user - only those who have given at least one vote
  $votingUserCount = $votingDao->getVotingUserCount($code, 'Nepodařilo se načíst počet hlasujících.');

  echo "Počet hlasujících: " . $votingUserCount;
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
?>
