<?php
include('VotingDao.class.php');

try {
  $data = json_decode(file_get_contents("php://input"));
  $code = $data->votingCode;

  $votingDao = new VotingDao();

  //validate - voting with code exists
  $votingDao->getVotingId($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  //validate - voting ended
  $votingDao->getVotingDetailCanShowResult($code, 'Nelze zobrazit výsledky.');

  //counts voting user - only those who have given at least one vote
  $votingUserCount = $votingDao->getVotingUserCount($code, 'Nepodařilo se načíst počet hlasujících.');

  $arr = $votingDao->getOptionListResult($code);

  $ret = new stdClass();
  $ret->votingUserCount = $votingUserCount;
  $ret->votingOptionList = $arr;

  echo $json_info = json_encode($ret);
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
?>
