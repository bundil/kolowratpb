<?php
include('VotingDao.class.php');

try {
  $data = json_decode(file_get_contents("php://input"));
  $code = $data->votingCode;

  $votingDao = new VotingDao();
  $voting = $votingDao->getVotingDetail($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  echo $json_info = json_encode($voting);
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
?>
