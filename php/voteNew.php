<?php
include('VotingDao.class.php');
include('Constant.class.php');
include('Utils.class.php');

try {
  $data = json_decode(file_get_contents("php://input"));
  $code = $data->votingCode;
  $email = $data->email;
  $pin = $data->pin;

  //VALIDATING
  //validate - email
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    throw new InvalidArgumentException('Emailová adresa ' . $email . ' nemá správný formát.');
  }
  //validate - max / min count plus / minus votes
  if ((count($data->votePlusArr) === 0) ||
    (count($data->votePlusArr) > Constant::VOTE_PLUS_MAX) ||
    (count($data->voteMinusArr) > Constant::VOTE_MINUS_MAX)) {
    throw new InvalidArgumentException('Nesprávný počet kladných, nebo záporných hlasů.');
  }
  //validate - minus votes only if there two times more plus votes
  if ((count($data->voteMinusArr) * 2) > count($data->votePlusArr)) {
    throw new InvalidArgumentException('Záporný hlas můžete udělit pouze v případě udělení ' . Constant::VOTE_MINUS_MAX . ' kladných hlasů.');
  }
  //validate - only number
  for ($i = 0; $i < count($data->votePlusArr); ++$i) {
    $votingOptionId = $data->votePlusArr[$i];
    if (!is_numeric($votingOptionId) || $votingOptionId < 1 || $votingOptionId != round($votingOptionId, 0)) {
      throw new InvalidArgumentException('Chyba na vstupu.');
    }
  }
  for ($i = 0; $i < count($data->voteMinusArr); ++$i) {
    $votingOptionId = $data->voteMinusArr[$i];
    if (!is_numeric($votingOptionId) || $votingOptionId < 1 || $votingOptionId != round($votingOptionId, 0)) {
      throw new InvalidArgumentException('Chyba na vstupu.');
    }
  }

  $votingDao = new VotingDao();
  $utils = new Utils();

  $ip = $utils->getClientIP();

  //validate - voting with code exists
  $votingId = $votingDao->getVotingId($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  //validate - is actual
  $votingDao->getVotingDetailActual($code, 'Nelze hlasovat - hlasování neprobíhá.');

  $votingUser = $votingDao->getVotingUser($code, $email, 'Nejprve je potřeba si pro daný email zaslat PIN.');

  if ($votingUser['pin_attempt_count'] >= Constant::PIN_ATTEMPT_MAX) {
    throw new InvalidArgumentException('Vyčerpán maximální počet pokusů pro zadání PINu.');
  }

  if (!empty($votingUser['vote_ip'])) {
    throw new InvalidArgumentException('Již jste hlasoval. Každý může hlasovat pouze jednou.');
  }

  $votingUserId = $votingUser['id'];
  if ($pin != $votingUser['pin']) {
    $votingDao->updateVotingUserIncrementPinAttemptCount($votingUserId);
    throw new InvalidArgumentException('PIN není správný.');
  }

  //all is ok
  $votingDao->updateVotingUserVote($votingUserId, $ip);

  $sign = true;
  for ($i = 0; $i < count($data->votePlusArr); ++$i) {
    $votingOptionId = $data->votePlusArr[$i];
    $votingDao->insertVote($votingUserId, $votingOptionId, $sign);
  }

  $sign = false;
  for ($i = 0; $i < count($data->voteMinusArr); ++$i) {
    $votingOptionId = $data->voteMinusArr[$i];
    $votingDao->insertVote($votingUserId, $votingOptionId, $sign);
  }

  echo 'true';
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
