<?php

use PHPMailer\PHPMailer\PHPMailer;

require './lib/PHPMailer/src/PHPMailer.php';
require './lib/PHPMailer/src/SMTP.php';

include('VotingDao.class.php');
include('Utils.class.php');

try {
  $data = json_decode(file_get_contents("php://input"));
  $code = $data->votingCode;
  $email = $data->email;

  //validate - email
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    throw new InvalidArgumentException('Emailová adresa ' . $email . ' nemá správný formát.');
  }

  $votingDao = new VotingDao();
  $utils = new Utils();

  $ip = $utils->getClientIP();

  $votingId = $votingDao->getVotingId($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  $voting = $votingDao->getVotingDetailActual($code, 'Nelze hlasovat - hlasování neprobíhá.');

  $votingUserExists = false;
  try {
    $votingUser = $votingDao->getVotingUser($code, $email, 'Uživatel ještě neexistuje.');
    $votingUserExists = true;
    //user exists - only resend PIN
    $votingUserId = $votingUser['id'];
    $pin = $votingUser['pin'];
  } catch (Exception $e) {
    $votingUserExists = false;
    //user does not exists - generate and send PIN, create new user
    $pin = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
  }

  if (!empty($votingUser['vote_ip'])) {
    throw new InvalidArgumentException('Již jste hlasoval. Každý může hlasovat pouze jednou.');
  }

  $ini = parse_ini_file("config.ini");
  $dbHost = $ini['db_host'];

  //SEND MAIL
  $mail = new PHPMailer();
  //Server settings
  $mail->CharSet = 'UTF-8';
  $mail->isSMTP();
  $mail->Host = $ini['mail_host'];
  $mail->Port = $ini['mail_port'];
  $mail->SMTPAuth = $ini['mail_smtpAuth'];
  $mail->Username = $ini['mail_username'];
  $mail->Password = $ini['mail_password'];
  $mail->SMTPSecure = $ini['mail_smtpSecure'];

  //Recipients
  $mail->setFrom($ini['mail_from']);
  $mail->addAddress($email);

  //Content
  $mail->Subject = 'PIN pro hlasovaní Kolovraty';
  $mail->Body = 'PIN pro hlasování Kolovraty - ' . $voting['question'] . ': ' . $pin;
  if (!$mail->send()) {
    throw new Exception("Nepodařilo se odeslat email s pinem.");
  }

  if (!$votingUserExists) {
    $votingUserId = $votingDao->insertVotingUser($votingId, $email, $pin, $ip);
  }

  echo 'true';
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
