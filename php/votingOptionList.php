<?php
include('VotingDao.class.php');

try {
  $data = json_decode(file_get_contents("php://input"));
  $code = $data->votingCode;

  $votingDao = new VotingDao();

  //validate - voting with code exists
  $votingId = $votingDao->getVotingId($code, 'Hlasování s kódem: ' . $code . ' neexistuje');

  //validate - voting is actual
  $voting = $votingDao->getVotingDetailActual($code, 'Nelze hlasovat - hlasování neprobíhá.');

  $arr = $votingDao->getOptionList($code);

  echo $json_info = json_encode($arr);
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
?>
