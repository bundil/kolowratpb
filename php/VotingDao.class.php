<?php

class VotingDao
{
  private $conn;

  function __construct()
  {
    $ini = parse_ini_file("config.ini");
    $dbHost = $ini['db_host'];
    $dbName = $ini['db_name'];

    $options = array(
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    );

    $this->conn = @new PDO("mysql:host=$dbHost;dbname=$dbName", $ini["db_username"], $ini['db_password'], $options);
  }

  private function query($query, $param = Array())
  {
    $stmt = $this->conn->prepare($query);
    $stmt->execute($param);
    return $stmt->rowCount();
  }

  private function queryForObject($query, $param = Array(), $errText)
  {
    $stmt = $this->conn->prepare($query);
    $stmt->execute($param);
    if ($stmt->rowCount() != 1) {
      throw new UnexpectedValueException($errText);
    }
    return $stmt->fetch(PDO::FETCH_ASSOC);
  }

  private function queryForList($query, $param = Array())
  {
    $stmt = $this->conn->prepare($query);
    $stmt->execute($param);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  private function insert($query, $param = Array())
  {
    $stmt = $this->conn->prepare($query);
    $stmt->execute($param);
    return $this->conn->lastInsertId();
  }

  private function update($query, $param = Array())
  {
    $stmt = $this->conn->prepare($query);
    $stmt->execute($param);
    return $stmt->rowCount();
  }

  //-------------------------------------------------------------------------

  function getVotingId($code, $errText)
  {
    $query = "SELECT id FROM Voting WHERE code=?";
    $row = $this->queryForObject($query, array($code), $errText);
    return $row["id"];
  }

  function getVotingDetail($code, $errText)
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting WHERE code=?";
    return $this->queryForObject($query, array($code), $errText);
  }

  function getVotingDetailActual($code, $errText)
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting WHERE code=? AND NOW() between voting_start and voting_end";
    return $this->queryForObject($query, array($code), $errText);
  }

  function getVotingDetailCanShowResult($code, $errText)
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting WHERE code=? AND NOW() > show_result_from";
    return $this->queryForObject($query, array($code), $errText);
  }

  function getVotingList()
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting ORDER BY id ASC";
    return $this->queryForList($query);
  }

  function getVotingListActual()
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting WHERE NOW() between voting_start and voting_end ORDER BY id ASC";
    return $this->queryForList($query);
  }

  function getVotingListCanShowResult()
  {
    $query = "SELECT code, question, text, DATE_FORMAT(voting_start,'%d.%m.%Y') AS start, DATE_FORMAT(voting_end,'%d.%m.%Y') AS end, created FROM Voting WHERE NOW() > show_result_from ORDER BY id ASC";
    return $this->queryForList($query);
  }

  function getOptionList($code)
  {
    $query = "SELECT vo.id, vo.option_text, vo.option_description FROM Voting v, Voting_option vo WHERE v.id = vo.voting_id AND code=? ORDER BY vo.id ASC";
    return $this->queryForList($query, array($code));
  }

  function getOptionListResult($code)
  {
    $query = "SELECT vo.id, vo.option_text, vo.option_description, (SELECT count(id) FROM Vote WHERE voting_option_id = vo.id AND sign = TRUE) AS votePlusCount, (SELECT count(id) FROM Vote WHERE voting_option_id = vo.id AND sign = FALSE) AS voteMinusCount FROM Voting v, Voting_option vo WHERE v.id = vo.voting_id AND code=? ORDER BY (votePlusCount - voteMinusCount) DESC";
    return $this->queryForList($query, array($code));
  }

  //counts voting user - only those who have given at least one vote
  function getVotingUserCount($code, $errText)
  {
    $query = "SELECT count(*) as votingUserCount FROM Voting_user vu, Voting v WHERE v.id = vu.voting_id AND v.code = ? AND EXISTS (SELECT * FROM Vote vo WHERE vu.id = vo.voting_user_id)";
    $row = $this->queryForObject($query, array($code), $errText);
    return $row["votingUserCount"];
  }

  function getVotingUser($code, $email, $errText)
  {
    $query = "SELECT vu.id, vu.voting_id, vu.email, vu.pin, vu.ip, vu.created, vu.vote_ip, vu.vote_created, vu.pin_attempt_count FROM Voting_user vu, Voting v WHERE v.id = vu.voting_id AND v.code = ? AND vu.email = ?";
    return $this->queryForObject($query, array($code, $email), $errText);
  }

  function insertVotingUser($votingId, $email, $pin, $ip)
  {
    $query = "INSERT INTO Voting_user (voting_id, email, pin, ip, created, pin_attempt_count) VALUES (?, ?, ?, ?, NOW(), 0)";
    return $this->insert($query, array($votingId, $email, $pin, $ip));
  }

  function updateVotingUserIncrementPinAttemptCount($id)
  {
    $query = "UPDATE Voting_user SET pin_attempt_count = pin_attempt_count + 1, created = created WHERE id = ?";
    return $this->update($query, array($id));
  }

  function updateVotingUserVote($id, $vote_ip)
  {
    $query = "UPDATE Voting_user SET vote_ip = ?, vote_created = NOW(), created = created WHERE id = ?";
    return $this->update($query, array($vote_ip, $id));
  }

  function insertVote($votingUserId, $votingOptionId, $sign)
  {
    $query = "INSERT INTO Vote (voting_user_id, voting_option_id, sign) VALUES (?, ?, ?)";
    return $this->insert($query, array($votingUserId, $votingOptionId, $sign));
  }
}

?>
