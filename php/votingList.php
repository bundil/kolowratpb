<?php
include('VotingDao.class.php');

try {
  $votingDao = new VotingDao();
  $votingListActual = $votingDao->getVotingListActual();
  $votingListEnded = $votingDao->getVotingListCanShowResult();

  $ret = new stdClass();
  $ret->votingListActual = $votingListActual;
  $ret->votingListEnded = $votingListEnded;

  echo $json_info = json_encode($ret);
} catch (Exception $e) {
  http_response_code(500);
  echo $e->getMessage();
}
?>
