# KolowratPB

Voting for democracy 21 (voting with 2 positive votes and 1 negative vote) for participatory budgeting.
Application authenticates user by sending PIN to his email. This way, it prevents duplicate voting.

## Demo
You cat try demo at http://kolowratpb-demo.zapodjezdem853.cz

## Used technologies
- Angular 5
- PHP
- MySQL

## Requirements
- webhosting with PHP, MySQL and SMTP mail server
- nodeJS and npm installed (version 10.x) - can be get at https://www.npmjs.com/get-npm

## Installation guide
- download project or clone GIT repository
- create DB
  - run *sql\V0001__init.sql* in MySQL
- edit texts
  - edit file *src\assets\i18n\cs.json* - you must change name Kolovraty to name of your city and change info email.
  - edit file *php\votingUserNew.php* - you must change name Kolovraty to name of your city.
- edit configs
  - edit angular config file *src\environments\environment.prod.ts* - set your host server (URL to PHP backend)
  - edit php config file *php\config.ini* - set your DB and mail smtp server connections
- change icon
  - you can change favicon.ico to change web page icon to logo of your city
- build
  - in base dir run:
    - *npm install*
    - *ng build --prod*
      - optional set subfolder URL with *--base-href=/subfolder/*
- copy files to server
  - copy content of *dist* directory to the server 
  - copy directory *php* to the server

## Add voting
Application doesn´t have admin section for adding new voting. But it is easy to do it in MySQL admin.

Add one row in voting table: 

 | table row | description | sample data  
 |------|----------|-------------
 | id | id of table row, unique per voting | 1 
 | code | code of voting - is used in URL, unique per voting | AA 
 | question | question of voting | favorite color 
 | text | additional text | Your favorite color. 
 | voting_start | when voting starts | 2019-05-01 00:00:00 
 | voting_end | when voting ends | 2019-06-01 00:00:00 
 | show_result_from | from when the results are shown | 2019-06-08 00:00:00 
 | created | created time | 2019-04-20 00:00:00 
 
Add more row in voting_option table: 

 | table row | description | sample data  
 |------|----------|-------------
 | id | id of table row, unique per voting option | 1 
 | voting_id | id of row of corresponding voting table | 1 
 | option_text | simple option text | red 
 | option_description | addition long description | ```<b>It is red color:</b><img src="https://yourhost.cz/img/AA/red/redImage.jpg">```

In this example, you can copy red image to /img/AA/red/redImage.jpg on your server, and it will be shown in the description of option.

## Force HTTPS and allow refresh page
To force https and allow refresh page with Apache you can add *.htaccess* (you should change hostname)
```
RewriteEngine On
    RewriteCond %{ENV:HTTPS} !^.*on
    RewriteRule ^(.*)$ https://yourhost.cz/$1 [R,L]

    # If an existing asset or directory is requested go to it as it is
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
    RewriteRule ^ - [L]
    # If the requested resource doesn't exist, use index.html
    RewriteRule ^ /index.html
```

## GDPR
In Europe because of GDPR one year after the end of the voting, you should anonymize rows: email, ip and vote_ip

You can use this sql:
UPDATE Voting_user set email = 'anonym@anonym.cz', ip = '0.0.0.0', vote_ip = '0.0.0.0' where voting_id = ?

## Contact
You can contact me at email bundil@seznam.cz, if you have any question.
