CREATE TABLE Voting
(
  id               INT          NOT NULL AUTO_INCREMENT,
  code             VARCHAR(100) NOT NULL,
  question         VARCHAR(500) NOT NULL,
  text             VARCHAR(65535),
  voting_start     DATETIME     NOT NULL,
  voting_end       DATETIME     NOT NULL,
  show_result_from TIMESTAMP    NOT NULL,
  created          TIMESTAMP    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Voting_option
(
  id                 INT            NOT NULL AUTO_INCREMENT,
  voting_id          INT            NOT NULL,
  option_text        VARCHAR(500)   NOT NULL,
  option_description VARCHAR(65535) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (voting_id) REFERENCES Voting (id)
);

CREATE TABLE Voting_user
(
  id                INT          NOT NULL AUTO_INCREMENT,
  voting_id         INT          NOT NULL,
  email             VARCHAR(100),
  phone             VARCHAR(100),
  pin               VARCHAR(100),
  ip                VARCHAR(100) NOT NULL,
  created           TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vote_ip           VARCHAR(100) DEFAULT NULL,
  vote_created      TIMESTAMP,
  pin_attempt_count INT          NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (voting_id) REFERENCES Voting (id)
);

CREATE TABLE Vote
(
  id               INT  NOT NULL AUTO_INCREMENT,
  voting_user_id   INT  NOT NULL,
  voting_option_id INT  NOT NULL,
  sign             BOOL NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (voting_user_id) REFERENCES Voting_user (id),
  FOREIGN KEY (voting_option_id) REFERENCES Voting_option (id)
);
